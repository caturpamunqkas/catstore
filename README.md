# Catstore
Dan tentunya aplikasi toko online ini dapat dengan mudah dimodifikasi sesuai dengan kebutuhan agar dapat diterapkan secara sempurna.

Fitur utama dari aplikasi toko online ini yaitu :
1. Penjualan produk
2. Pemesanan produk secara online
3. Pengolahan dan Managemen Produk
4. Informasi toko
5. Dan masih banyak lagi..

### Installing

```
git clone https://gitlab.com/caturpamungkas/catstore.git
```