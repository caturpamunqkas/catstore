<div class="row">
	<ol class="breadcrumb">
		<li><a href="#">
			<em class="fa fa-home"></em>
		</a></li>
		<li class="active">Kategori</li>
	</ol>
</div><!--/.row-->

<?php 
if (@$_GET['hasil']=='true') {
	echo "
	<div class='alert bg-success' role='alert'>
		<em class='fa fa-lg fa-warning'>&nbsp;</em> Data Berhasil Di Ubah.
		<a href='index.php?i=kategori' class='pull-right'>
			<em class='fa fa-lg fa-close'></em>
		</a>
	</div>
	";
}elseif(@$_GET['hasil']=='false'){
	echo "
	<div class='alert bg-danger' role='alert'>
		<em class='fa fa-lg fa-warning'>&nbsp;</em> Ada Yang Salah Saat Mengubah Data.
		<a href='index.php?i=kategori' class='pull-right'>
			<em class='fa fa-lg fa-close'></em>
		</a>
	</div>
	";
}
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Kategori</h1>
	</div>
</div><!--/.row-->

<div class="panel panel-container">
	<div class="row">
		<div class="col-md-12" style="padding-left: 30px; padding-right: 30px">
			<a href="index.php?action=tambah_bank">
				<button type="button" style="margin-bottom: 10px" class="btn btn-sm btn-success">Tambah Data</button>
			</a>
			<table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
			  <thead>
			    <tr>
			      <th class="th-sm">NO</th>
			      <th class="th-sm">Nama Pemilik</th>
			      <th class="th-sm">No.Rekening</th>
			      <th class="th-sm">Nama Bank</th>
			      <th class="th-sm">Aksi</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php
				  	$no = 1;
				  	$qry = mysql_query("SELECT * FROM bank"); 
			  		while ($row = mysql_fetch_assoc($qry)) {
			  	?>
			    <tr>
			      <td><?php echo $no++; ?></td>
			      <td><?php echo $row['nama']; ?></td>
			      <td><?php echo $row['no_rek']; ?></td>
			      <td><?php echo $row['bank']; ?></td>
			      <td>
			      	<a href="index.php?action=edit_bank&id=<?php echo $row['id'];?>"><button type="button" class="btn btn-sm btn-default">Edit</button></a>
			      	<a href="action.php?action=delete_bank&id=<?php echo $row['id']; ?>"><button type="button" class="btn btn-sm btn-danger">Hapus</button></a>
			      </td>
			    </tr>
			    <?php } ?>
			  </tbody>
			</table>
		</div>
	</div>
</div>
