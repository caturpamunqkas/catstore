<div class="row">
	<ol class="breadcrumb">
		<li><a href="#">
			<em class="fa fa-home"></em>
		</a></li>
		<li class="active">Dashboard</li>
	</ol>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Dashboard</h1>
	</div>
</div><!--/.row-->

<div class="panel panel-container">
	<div class="row">
		<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
			<div class="panel panel-teal panel-widget border-right">
				<div class="row no-padding"><em class="fa fa-xl fa-shopping-cart color-blue"></em>
					<div class="large">120</div>
					<div class="text-muted">New Orders</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
			<div class="panel panel-blue panel-widget border-right">
				<div class="row no-padding"><em class="fa fa-xl fa-comments color-orange"></em>
					<div class="large">52</div>
					<div class="text-muted">Comments</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
			<div class="panel panel-orange panel-widget border-right">
				<div class="row no-padding"><em class="fa fa-xl fa-users color-teal"></em>
					<div class="large"><?php echo $data_user; ?></div>
					<div class="text-muted">New Users</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
			<div class="panel panel-red panel-widget ">
				<div class="row no-padding"><em class="	fa fa-cube fa-xl color-red"></em>
					<div class="large"><?php echo $data_produk; ?></div>
					<div class="text-muted">Produk</div>
				</div>
			</div>
		</div>
	</div><!--/.row-->
</div>