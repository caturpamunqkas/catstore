<?php
include '../includes/koneksi.php';

session_start();
if(!$_SESSION['status']=="adminLogin"){
	header("location:login.php");
}

// Menghitung Jumlah User
$data_user = mysql_num_rows(mysql_query("SELECT * FROM users"));

// Mengambil Jumlah Produk
$data_produk = mysql_num_rows(mysql_query("SELECT * FROM produk"));
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Lumino - Dashboard</title>
	<link href="../includes/admin/css/bootstrap.min.css" rel="stylesheet">
	<link href="../includes/admin/css/font-awesome.min.css" rel="stylesheet">
	<link href="../includes/admin/css/datepicker3.css" rel="stylesheet">
	<link href="../includes/admin/css/styles.css" rel="stylesheet">
	<link href="../includes/admin/css/custom.css" rel="stylesheet">
	<!-- MDBootstrap Datatables  -->
	<link href="../includes/admin/cssaddons/datatables.min.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="#"><span>Catstore</span>Admin</a>
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-envelope"></em><span class="label label-danger">15</span>
					</a>
						<ul class="dropdown-menu dropdown-messages">
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
									</a>
									<div class="message-body"><small class="pull-right">3 mins ago</small>
										<a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
									<br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
									</a>
									<div class="message-body"><small class="pull-right">1 hour ago</small>
										<a href="#">New message from <strong>Jane Doe</strong>.</a>
									<br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="all-button"><a href="#">
									<em class="fa fa-inbox"></em> <strong>All Messages</strong>
								</a></div>
							</li>
						</ul>
					</li>
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-bell"></em><span class="label label-info">5</span>
					</a>
						<ul class="dropdown-menu dropdown-alerts">
							<li><a href="#">
								<div><em class="fa fa-envelope"></em> 1 New Message
									<span class="pull-right text-muted small">3 mins ago</span></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-heart"></em> 12 New Likes
									<span class="pull-right text-muted small">4 mins ago</span></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-user"></em> 5 New Followers
									<span class="pull-right text-muted small">4 mins ago</span></div>
							</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>

	<?php include '../includes/sidebar.php'; ?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">

		<?php
			if (@$_GET['i']=='dashboard') {
				include 'dashboard.php';
			}elseif (@$_GET['i']=='kategori') {
				include 'kategori.php';
			}elseif (@$_GET['action']=='edit_kategori' && @$_GET['id']) {
				include 'input_kategori.php';
			}elseif (@$_GET['action']=='tambah_kategori') {
				include 'input_kategori.php';
			}elseif (@$_GET['i']=='produk') {
				include 'produk.php';
			}elseif (@$_GET['action']=='edit_produk' && @$_GET['id']) {
				include 'input_produk.php';
			}elseif (@$_GET['action']=='tambah_produk') {
				include 'input_produk.php';
			}elseif (@$_GET['i']=='kurir') {
				include 'kurir.php';
			}elseif (@$_GET['action']=='edit_kurir' && @$_GET['id']) {
				include 'input_kurir.php';
			}elseif (@$_GET['action']=='tambah_kurir') {
				include 'input_kurir.php';
			}elseif (@$_GET['i']=='bank') {
				include 'bank.php';
			}elseif (@$_GET['action']=='edit_bank' && @$_GET['id']) {
				include 'input_bank.php';
			}elseif (@$_GET['action']=='tambah_bank') {
				include 'input_bank.php';
			}elseif (@$_GET['i']=='transaksi') {
				include 'transaksi.php';
			}elseif (@$_GET['action']=='transaksi_detail' && @$_GET['id']) {
				include 'transaksi_detail.php';
			}elseif (@$_GET['action']=='edit_transaksi' && @$_GET['id']) {
				include 'input_transaksi.php';
			}elseif (@$_GET['action']=='tambah_transaksi') {
				include 'input_transaksi.php';
			}elseif (@$_GET['i']=='config') {
				include 'input_config.php';
			}elseif(@$_GET['i']=='ulasan') {
				include 'ulasan.php';
			}else{
				include 'dashboard.php';
			}
		?>

	</div>	<!--/.main-->
	
	<script src="../includes/admin/js/jquery-1.11.1.min.js"></script>
	<script src="../includes/admin/js/bootstrap.min.js"></script>
	<script src="../includes/admin/js/chart.min.js"></script>
	<script src="../includes/admin/js/chart-data.js"></script>
	<script src="../includes/admin/js/easypiechart.js"></script>
	<script src="../includes/admin/js/easypiechart-data.js"></script>
	<script src="../includes/admin/js/bootstrap-datepicker.js"></script>
	<script src="../includes/admin/js/custom.js"></script>
	<!-- MDBootstrap Datatables  -->
	<script type="text/javascript" src="../includes/admin/js/addons/datatables.min.js"></script>
	<script src="../includes/admin/js/jquery.mask.min.js"></script>
	<script type="text/javascript">
		function del($id){
			$(document).ready(function(){
			    $.ajax({url: "action.php?ulasan=delete&id="+$id, success: function(result){
			      location.reload();
			      alert("Berhasil Menghapus Ulasan");
			    }});
			});
		}
	</script>
	
</html>