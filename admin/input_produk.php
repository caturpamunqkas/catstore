<?php
if (@$_GET['id'] != '') {
	$qry = mysql_fetch_assoc(mysql_query("SELECT * FROM produk WHERE id='$_GET[id]'"));
}
?>
<div class="row">
	<ol class="breadcrumb">
		<li><a href="#">
			<em class="fa fa-home"></em>
		</a></li>
		<li class="active">Produk</li>
		<?php
		if (@$_GET['id']) {
			echo "<li class='active'>Edit Produk</li>";
		}else{
			echo "<li class='active'>Tambah Produk</li>";
		}
		?>
	</ol>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Produk</h1>
	</div>
</div><!--/.row-->

<div class="panel panel-container">
	<div class="row">
		<div class="col-lg-12">	
			<div class="panel-body">
				<div class="col-md-12">
					<form role="form" method="post" enctype="multipart/form-data" action="<?php if($_GET['action']=='edit_produk'){echo "action.php?action=$_GET[action]&id=$_GET[id]";}else{echo "action.php?action=$_GET[action]";} ?>">
						<div class="form-group">
							<label>Nama</label>
							<input class="form-control" required value="<?php if(@$_GET['id']){echo $qry['nama'];} ?>" name="nama" placeholder="">
						</div>
						<div class="form-group">
							<label>Deskripsi</label>
							<input class="form-control" required value="<?php if(@$_GET['id']){echo $qry['deskripsi'];} ?>" name="deskripsi" placeholder="">
						</div>
						<div class="form-group">
							<label>Kategori</label>
							<select class="form-control" required name="kategori">
								<option value="0">-- Pilih Kategori --</option>
								<?php 
								$ambil = mysql_query("SELECT * FROM kategori");
								while ($data=mysql_fetch_assoc($ambil)) { ?>
								<option value="<?php echo $data['id']; ?>" <?php if(@$_GET['id'] && $qry['id_kategori']==$data['id']){echo "selected";}?>><?php echo $data['nama']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label>Harga</label>
							<input class="form-control" required value="<?php if(@$_GET['id']){echo $qry['harga'];} ?>" id="harga" name="harga" placeholder="">
						</div>
						<div class="form-group">
							<label>Stok</label>
							<input class="form-control" required value="<?php if(@$_GET['id']){echo $qry['stok'];} ?>" name="stok" placeholder="">
						</div>
						<div class="form-group">
							<label>Status</label>
							<div class="checkbox">
								<label class="container"><div style="padding-left: 10px">Simpan Ke Draft</div>
									<input type="checkbox" id="status" name="status" <?php if(@$_GET['id']&&$qry['status']=='on'){echo 'checked';} ?>/>
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="form-group">
							<label>Gambar</label>
							<input class="form-control" name="foto" type="file">
						</div>
						<?php
						if (@$_GET['action']=='edit_produk') {
							echo	"<button type='submit' class='btn btn-sm btn-primary'>Ubah</button>";
						}else{
							echo	"<button type='submit' class='btn btn-sm btn-success'>Simpan</button>";
						}
						?>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

