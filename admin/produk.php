<div class="row">
	<ol class="breadcrumb">
		<li><a href="#">
			<em class="fa fa-home"></em>
		</a></li>
		<li class="active">Produk</li>
	</ol>
</div><!--/.row-->

<?php 
if (@$_GET['hasil']=='true') {
	echo "
	<div class='alert bg-success' role='alert'>
		<em class='fa fa-lg fa-warning'>&nbsp;</em> Data Berhasil Di Ubah.
		<a href='index.php?i=produk' class='pull-right'>
			<em class='fa fa-lg fa-close'></em>
		</a>
	</div>
	";
}elseif(@$_GET['hasil']=='false'){
	echo "
	<div class='alert bg-danger' role='alert'>
		<em class='fa fa-lg fa-warning'>&nbsp;</em> Ada Yang Salah Saat Mengubah Data.
		<a href='index.php?i=produk' class='pull-right'>
			<em class='fa fa-lg fa-close'></em>
		</a>
	</div>
	";
}
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Produk</h1>
	</div>
</div><!--/.row-->

<div class="panel panel-container">
	<div class="row">
		<div class="col-md-12" style="padding-left: 30px; padding-right: 30px">
			<a href="index.php?action=tambah_produk">
				<button type="button" style="margin-bottom: 10px" class="btn btn-sm btn-success">Tambah Data</button>
			</a>
			<table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
			  <thead>
			    <tr>
			      <th class="th-sm">NO</th>
			      <th class="th-sm">Gambar</th>
			      <th class="th-sm">Nama</th>
			      <th class="th-sm">Deskripsi</th>
			      <th class="th-sm">Kategori</th>
			      <th class="th-sm">Harga</th>
			      <th class="th-sm">Stok</th>
			      <th class="th-sm">Status</th>
			      <th class="th-sm">Action</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php
				  	$no = 1;
				  	// $qry = mysql_query("SELECT * FROM produk");
				  	$qry = mysql_query("SELECT A.foto, A.nama, A.deskripsi, A.harga, A.stok, A.status, A.id, B.nama AS nama_kat FROM produk AS A INNER JOIN kategori AS B ON (A.id_kategori = B.id)");
			  		while ($row = mysql_fetch_assoc($qry)) {
			  	?>
			    <tr>
			      <td><?php echo $no++; ?></td>
			      <td><img width="60px" src="../includes/images/produk/<?php echo $row['foto']; ?>"></td>
			      <td><?php echo $row['nama']; ?></td>
			      <td><?php echo $row['deskripsi']; ?></td>
			      <td><?php echo $row['nama_kat']; ?></td>
			      <td><?php echo rupiah($row['harga']); ?></td>
			      <td><?php echo $row['stok']; ?></td>
			      <td>
			      	<?php
			      		if ($row['status']=='on') {
			      			echo "<div style='text-align:center' class='btn-sm btn-default'>Disimpan</div>";
			      		}else{
			      			echo "<div style='text-align:center' class='btn-sm btn-success'>Publish</div>";
			      		}
			      	?>	
			      </td>
			      <td>
			      	<a href="index.php?action=edit_produk&id=<?php echo $row['id'];?>"><button type="button" class="btn btn-sm btn-default">Edit</button></a>
			      	<a href="action.php?action=delete_produk&id=<?php echo $row['id']; ?>"><button type="button" class="btn btn-sm btn-danger">Hapus</button></a>
			      </td>
			    </tr>
			    <?php } ?>
			  </tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
		$(document).ready(function () {
		  $('#dtBasicExample').DataTable();
		  $('.dataTables_length').addClass('bs-select');
		});
	</script>
