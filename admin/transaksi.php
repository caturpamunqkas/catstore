<div class="row">
	<ol class="breadcrumb">
		<li><a href="#">
			<em class="fa fa-home"></em>
		</a></li>
		<li class="active">Transaksi</li>
	</ol>
</div><!--/.row-->

<?php 
if (@$_GET['hasil']=='true') {
	echo "
	<div class='alert bg-success' role='alert'>
		<em class='fa fa-lg fa-warning'>&nbsp;</em> Data Berhasil Di Ubah.
		<a href='index.php?i=produk' class='pull-right'>
			<em class='fa fa-lg fa-close'></em>
		</a>
	</div>
	";
}elseif(@$_GET['hasil']=='false'){
	echo "
	<div class='alert bg-danger' role='alert'>
		<em class='fa fa-lg fa-warning'>&nbsp;</em> Ada Yang Salah Saat Mengubah Data.
		<a href='index.php?i=produk' class='pull-right'>
			<em class='fa fa-lg fa-close'></em>
		</a>
	</div>
	";
}
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Transaksi</h1>
	</div>
</div><!--/.row-->

<div class="panel panel-container">
	<div class="row">
		<div class="col-md-12" style="padding-left: 30px; padding-right: 30px">
			<!-- <a href="index.php?action=tambah_produk">
				<button type="button" style="margin-bottom: 10px" class="btn btn-sm btn-success">Tambah Data</button>
			</a> -->
			<table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
			  <thead>
			    <tr>
			      <th class="th-sm">NO</th>
			      <th class="th-sm">Nama</th>
			      <!-- <th class="th-sm">Alamat</th> -->
			      <th class="th-sm">Kurir</th>
			      <th class="th-sm">Total</th>
			      <th class="th-sm">Tanggal</th>
			      <th class="th-sm">Status</th>
			      <th class="th-sm">Action</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php
				  	$no = 1;
				  	// $qry = mysql_query("SELECT * FROM produk");
				  	$qry = mysql_query("SELECT P.tanggal, P.id_user, P.id, U.nama, K.kota, K.nama AS nama_kurir, B.nama AS nama_bank, B.bank, B.no_rek, K.harga, P.total, P.alamat, P.status FROM pembelian AS P 
						JOIN users AS U ON(P.id_user=U.id)
						JOIN bank AS B ON(P.id_bank=B.id)
						JOIN kurir AS K ON(P.id_kurir=K.id) ORDER BY P.tanggal DESC");
			  		while ($row = mysql_fetch_assoc($qry)) {
			  	?>
			    <tr>
			      <td><?php echo $no++; ?></td>
			      <td><?php echo $row['nama']; ?></td>
			      <!-- <td><?php echo $row['alamat']; ?></td> -->
			      <td><?php echo $row['kota']; ?><br><?php echo $row['nama_kurir']; ?></td>
			      <td><?php echo rupiah($row['total']); ?></td>
			      <td><?php echo tgl_indo($row['tanggal']); ?></td>
			      <td>
			      <?php if ($row['status']=='waiting') {
						echo "<span class='btn btn-sm btn-danger'>Belum Dibayar</span>";
					}elseif ($row['status']=='pending') {
						echo "<span class='btn btn-sm btn-success'>Sudah Dibayar</span>";
					}elseif ($row['status']=='proses') {
						echo "<span class='btn btn-sm btn-info'>Proses Packing</span>";
					}elseif ($row['status']=='kirim') {
						echo "<span class='btn btn-sm btn-info'>Barang Dikirim</span>";
					}else{
						echo "Pesanan <strong>DIBATALKAN</strong></li>";
					} ?>
			      <td>
			      	<a href="index.php?action=transaksi_detail&id=<?php echo $row['id'];?>"><button type="button" class="btn btn-sm btn-default">Lihat</button></a>
			      </td>
			    </tr>
			    <?php } ?>
			  </tbody>
			</table>
		</div>
	</div>
</div>
