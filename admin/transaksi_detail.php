<?php 
$qry = mysql_query("SELECT P.tanggal, P.id_user, P.resi, U.nama, K.kota, B.nama AS nama_bank, B.bank, B.no_rek, K.harga, P.total, P.alamat, P.status FROM pembelian AS P 
	JOIN users AS U ON(P.id_user=U.id)
	JOIN bank AS B ON(P.id_bank=B.id)
	JOIN kurir AS K ON(P.id_kurir=K.id) WHERE P.id = '$_GET[id]'");
$pembelian = mysql_fetch_assoc($qry);
?>
<div class="row">
	<ol class="breadcrumb">
		<li><a href="#">
			<em class="fa fa-home"></em>
		</a></li>
		<li class="active">Transaksi</li>
		<li class="active">Detail</li>
	</ol>
</div><!--/.row-->

<?php 
if (@$_GET['hasil']=='true') {
	echo "
	<div class='alert bg-success' role='alert'>
		<em class='fa fa-lg fa-warning'>&nbsp;</em> Data Berhasil Di Ubah.
		<a href='index.php?i=produk' class='pull-right'>
			<em class='fa fa-lg fa-close'></em>
		</a>
	</div>
	";
}elseif(@$_GET['hasil']=='false'){
	echo "
	<div class='alert bg-danger' role='alert'>
		<em class='fa fa-lg fa-warning'>&nbsp;</em> Ada Yang Salah Saat Mengubah Data.
		<a href='index.php?i=produk' class='pull-right'>
			<em class='fa fa-lg fa-close'></em>
		</a>
	</div>
	";
}
?>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">Detail Transaksi</h3>
	</div>
</div><!--/.row-->

<div class="panel panel-container">
	<div class="row">
		<div class="col-lg-12">
			<div class="col-md-7">
				<div class="panel-box-content">
					<ul class="list list-unstyled no-mar-bot mb-none">
						<li><strong>NAMA PENERIMA : </strong> <?php echo $pembelian['nama']; ?></li>
						<li><strong>DIKIRIM KE : </strong><?php echo $pembelian['alamat']; ?></li>
						<li><strong>TGL PEMESANAN : </strong><?php echo tgl_indo($pembelian['tanggal']); ?></li>
						<?php if(!$pembelian['resi']==null) {?>
						<li><strong>NO. RESI : </strong><font color="green"><?php echo $pembelian['resi']; ?></font>
						<?php } ?>
						<li><strong>STATUS : </strong>
						<?php if ($pembelian['status']=='waiting') {
								echo "<span class='btn btn-sm btn-danger'>Belum Dibayar</span>";
							}elseif ($pembelian['status']=='pending') {
								echo "<span class='btn btn-sm btn-success'>Sudah Dibayar</span>";
							}elseif ($pembelian['status']=='proses') {
								echo "<span class='btn btn-sm btn-info'>Proses Packing</span>";
							}elseif ($pembelian['status']=='kirim') {
								echo "<span class='btn btn-sm btn-info'>Barang Dikirim</span>";
							}else{
								echo "Pesanan <strong>DIBATALKAN</strong></li>";
							} ?>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-5">
				<div class="panel-box-content">
					<ul class="list list-unstyled no-mar-bot mb-none">
						<li><strong>TRANSFER KE : </strong> <?php echo $pembelian['bank']; ?></li>
						<li><strong>NO. REKENING : </strong><?php echo $pembelian['no_rek']; ?></li>
						<li><strong>A/N : </strong> <?php echo $pembelian['nama_bank']; ?></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding-left: 30px; padding-right: 30px">
			<!-- <a href="index.php?action=tambah_produk">
				<button type="button" style="margin-bottom: 10px" class="btn btn-sm btn-success">Tambah Data</button>
			</a> -->
			<table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Gambar</th>
						<th>Nama Produk</th>
						<th>Kuantitas</th>
						<th>Harga Produk</th>
						<th>Sub Total</th>
					</tr>
				</thead>
				<tbody>
					<?php $qty = mysql_query("SELECT * FROM qty_pembelian AS qty 
						JOIN produk AS pro ON(qty.id_produk=pro.id) WHERE id_pembelian='$_GET[id]'");
					while ($data = mysql_fetch_assoc($qty)) { 
					$kat = mysql_fetch_assoc(mysql_query("SELECT nama FROM kategori WHERE id='$data[id_kategori]'")); ?>
					<tr>
						<td class="product-image-td">
							<a href="javascript:void(0)" title="Nama Produk">
								<img width="100px" src="../includes/images/produk/<?php echo $data['foto']; ?>" alt="Nama Produk">
							</a>
						</td>
						<td class="product-name-td">
							<h5 class="product-name"><?php echo $data['nama']; ?></h5>
							<p><?php echo $kat['nama']; ?></p>
						</td>
						<td>
							<div class="qty-holder">
								<?php echo $data['qty']; ?> Produk
							</div>
						</td>
						<td><?php echo rupiah($data['harga']);?></td>
						<td><?php echo rupiah($data['harga']*$data['qty']);?></td>
					</tr>
					<?php } ?>
					<tr>
						<td colspan="4" class="clearfix">
							ONGKOS KIRIM - <strong><?php echo $pembelian['kota']; ?></strong>
						</td>
						<td class="clearfix">
							<?php echo rupiah($pembelian['harga']);?>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<strong>TOTAL</strong>
						</td>
						<td>
							<?php echo rupiah($pembelian['total']);?>
						</td>
					</tr>
			</table>
		</div>
	</div>
</div>
<?php 
$qry = mysql_query("SELECT P.nama, P.no_rek, P.bank, P.jumlah, P.bukti, B.nama AS nama_bank, B.bank AS bank_tf, B.no_rek AS norek FROM pembayaran AS P JOIN bank AS B ON(P.id_bank=B.id) WHERE id_pembelian = '$_GET[id]'");
$data = mysql_fetch_assoc($qry);

if(mysql_num_rows($qry)=='1' && $pembelian['status']=='pending') {?>
<div class="panel panel-container">
	<div class="row">
		<div class="col-md-12" style="padding-left: 30px; padding-right: 30px">
			<h4 style="text-align: center;"><strong>BUKTI PEMBAYARAN</strong></h4>
			<div class="col-md-7">
				<div class="panel-box-content">
					<ul class="list list-unstyled no-mar-bot mb-none">
						<hr>
						<li><strong>NAMA : </strong> <?php echo $data['nama']; ?></li>
						<li><strong>NO. REKENING : </strong> <?php echo $data['no_rek']; ?></li>
						<li><strong>DARI BANK : </strong> <?php echo $data['bank']; ?></li>
						<li><strong>JUMLAH TRANSFER : </strong> <?php echo rupiah($data['jumlah']); ?></li>
						<li><hr></li>
						<li><strong>DITRANSFER KE : </strong> <?php echo $data['bank_tf']; ?></li>
						<li><strong>NO. REKENING : </strong> <?php echo $data['norek']; ?></li>
						<li><strong>A/N : </strong> <?php echo $data['nama_bank']; ?></li>
						<hr>
					</ul>
				</div>
			</div>
			<div class="col-md-5">
				<div class="panel-box-content">
					<ul class="list list-unstyled no-mar-bot mb-none">
						<hr>
						<li><img src="../includes/images/bukti/<?php echo $data['bukti']; ?>"></li>
					</ul>
					<form method="post" action="action.php?pembayaran=konfirmasi">
						<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
						<button type="submit" class="btn btn-sm btn-success center">Konfirmasi Pembayaran</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if(mysql_num_rows($qry)=='1' && $pembelian['status']=='proses') {?>
<div class="panel panel-container">
	<div class="row">
		<div class="col-md-12" style="padding-left: 30px; padding-right: 30px">
			<h4 style="text-align: center;"><strong>INPUTKAN RESI</strong></h4>
			<form method="post" action="action.php?pembayaran=dikirim">
				<div class="form-group">
					<label>NO. RESI</label>
					<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
					<input class="form-control" name="resi">
				</div>
				<button type='submit' class='btn btn-sm btn-primary'>Kirim</button>
			</form>
		</div>
	</div>
</div>
<?php } ?>

