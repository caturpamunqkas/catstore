<?php

?>
<div class="cart-dropdown">
	<a href="index.php?i=cart" class="cart-dropdown-icon">
		<i class="minicart-icon"></i>
		<span class="cart-info">
			<span class="cart-qty"><?php echo count(@$_SESSION["cart"]); ?></span>
			<span class="cart-text">Barang</span>
		</span>
	</a>
	<div class="cart-dropdownmenu right">
		<div class="dropdownmenu-wrapper">
			<?php
			$totalharga= 0;
			if(is_array(@$_SESSION["cart"])){
			foreach (@$_SESSION["cart"] as $id_produk => $jumlah) {
			$qry = mysql_query("SELECT * FROM produk WHERE id = '$id_produk'");
			$i = mysql_fetch_assoc($qry);
			$total = $i["harga"]*$jumlah;
			$totalharga+=$total
			?>
			<div class="cart-products">
				<div class="product product-sm">
					<a href="javascript:void(0)" onclick="deltocart(<?php echo $i['id']; ?>)" class="btn-remove" title="Buang item">
						<i class="fa fa-times"></i>
					</a>
					<figure class="product-image-area">
						<a href="includes/main/javascript:void(0)" title="Nama Produk" class="product-image">
							<img src="includes/main/img/demos/shop/products/thumbs/cart-product1.jpg" alt="Nama Produk">
						</a>
					</figure>
					<div class="product-details-area">
						<h2 class="product-name">
							<a href="includes/main/javascript:void(0)" title="Nama Produk"><?php echo $i['nama']; ?></a>
						</h2>
						<div class="cart-qty-price">
							<?php echo $jumlah; ?> X
							<span class="product-price"><?php echo rupiah($i['harga']); ?></span>
						</div>
					</div>
				</div>
			</div>
			<?php }
			} ?>

			<?php 
			if (@$_SESSION["cart"]) {
			?>
			<div class="cart-totals">
				Total: <span><?php echo rupiah($totalharga); ?></span>
			</div>
			<?php }else{ ?>
			<div class="cart-totals">
				Cart Anda Kosong
			</div>
			<?php } ?>

			<?php if (@$_SESSION["cart"]) { ?>
			<div class="cart-actions">
				<a href="index.php?i=cart" class="btn btn-primary">Lihat Keranjang</a>
				<?php if (@$_SESSION['status']=='userLogin') { ?>
					<a href="index.php?i=checkout" class="btn btn-primary">Bayar Sekarang</a>
				<?php }else{ ?>
					<a href="index.php?i=login" style="padding-left: 6px" class="btn btn-primary">Login Untuk Bayar</a>
				<?php } ?>
			</div>
			<?php } ?>

		</div>
	</div>
</div>