<div role="main" class="main">
	<div class="cart">
		<div class="container">
			<h1 class="h2 heading-primary mt-lg clearfix">
				<span>Troli Belanja (<?php echo count(@$_SESSION["cart"]); ?> Produk)</span>
			</h1>
			<div class="row">
				<div class="col-md-8 col-lg-8">
					<div class="cart-table-wrap">
						<table class="cart-table">
							<thead>
								<tr>
									<th>&nbsp;</th>
									<th>Nama Produk</th>
									<th>Harga Produk</th>
									<th>Kuantitas</th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$totalharga= 0;
								if(is_array(@$_SESSION["cart"])){
								foreach (@$_SESSION["cart"] as $id_produk => $jumlah) {
								$qry = mysql_query("SELECT * FROM produk WHERE id = '$id_produk'");
								$i = mysql_fetch_assoc($qry);

								$qry1 = mysql_query("SELECT * FROM kategori WHERE id = '$i[id_kategori]'");
								$i1 = mysql_fetch_assoc($qry1);

								$total = $i["harga"]*$jumlah;
								$totalharga+=$total
								?>
								<tr>
									<td class="product-image-td">
										<a href="javascript:void(0)" title="Nama Produk">
											<img src="includes/images/produk/<?php echo $i['foto']; ?>" alt="Nama Produk">
										</a>
									</td>
									<td class="product-name-td">
										<h2 class="product-name"><a href="javascript:void(0)" title="Nama Produk"><?php echo $i['nama']; ?></a></h2>
										<p>Kategori : <?php echo $i1['nama']; ?></p>
										<p>Hanya Tersedia: <?php echo $i['stok']; ?></p>
										<!-- <a href="javascript:void(0)"><i class="fa fa-heart-o"></i> Pindahkan ke Wishlist</a> -->
									</td>
									<td><?php echo rupiah($i['harga']); ?></td>
									<td>
										<div class="qty-holder">
											<a href="javascript:void(0)" class="qty-dec-btn" title="Kurangi">-</a>
											<input type="text" id="inc-<?php echo $i['id']; ?>" class="qty-input" value="<?php echo $jumlah; ?>">
											<a href="javascript:void(0)" onclick="plus(<?php echo $i['id']; ?>)" class="qty-inc-btn" title="Tambah">+</a>
										</div>
									</td>
									<td class="product-action-td">
										<a href="javascript:void(0)" onclick="deltocart(<?php echo $i['id']; ?>)" title="Hapus Produk" class="btn-remove"><i class="fa fa-times"></i></a>
									</td>
								</tr>
								<?php }
								} ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-md-4 col-lg-4 sidebar shop-sidebar">
					<div class="panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a href="#rincian-pesanan" class="accordion-toggle collapsed" data-toggle="collapse">Rincian Pesanan</a>
								</h4>
							</div>
							<div class="accordion-body collapse in" id="rincian-pesanan">
								<div class="panel-body">
									<table class="totals-table">
										<tbody>
											<tr style="border-bottom: 0px;">
												<td>Subtotal</td>
												<td><?php echo rupiah($totalharga); ?></td>
											</tr>
												<?php 
												if (@$_SESSION['status']) {
													$dat = mysql_fetch_assoc(mysql_query("SELECT kota FROM users WHERE id = '$_SESSION[id]'"));
													$quer = mysql_query("SELECT * FROM kurir WHERE kota = '$dat[kota]'");
													$cek = mysql_num_rows($quer);
													$kurir = mysql_fetch_assoc($quer);

													echo "
													<tr>
													<td>Biaya Pengiriman";
														if (@$_SESSION['status'] && @$_SESSION['cart']) {
															echo " - ".$dat['kota'];
														}
													echo "</td>";
													echo "<td>";
													if (!$cek==0 && count(@$_SESSION['cart'])!='0') {
														echo rupiah($kurir['harga']);
													}else{
														echo "Rp. 0";
													}
													echo "</td>
														</tr>
													<tr>
														<td><strong>Total</strong></td>
														<td>";
													if (!$cek==0 && count(@$_SESSION['cart'])!='0') {
														echo rupiah($totalharga+$kurir['harga']);
													}else{
														echo "Rp. 0";
													}
													echo "</td>
													</tr>";
												}
												?>
										</tbody>
									</table>
									<div class="totals-table-action mt-sm">
										<?php if(@$_SESSION['status']=='userLogin') {?>
											<?php if($cek==0 && count(@$_SESSION['cart'])!='0') {	?>
												<button class="btn btn-primary btn-block text-uppercase" onclick="window.location.href='index.php?i=checkout'">Hubungi admin</button>
												<p>*) Hubungi Admin Untuk Meminta Biaya Kirim Ke Kota Anda</p>
											<?php }else{ ?>
												<?php if(count(@$_SESSION['cart'])!='0') { ?>
													<button class="btn btn-primary btn-block text-uppercase" onclick="window.location.href='index.php?i=checkout'">Lanjutkan ke Pembayaran</button>
												<?php }else{ ?>
													<button class="btn btn-primary btn-block text-uppercase" onclick="window.location.href='index.php?i=produk'">Silahkan Berbelanja</button>
												<?php } ?>
											<?php } ?>
										<?php }else{ ?>
											<button class="btn btn-primary btn-block text-uppercase" onclick="window.location.href='index.php?i=login'">Silahkan Login Terlebih Dahulu</button>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>