<?php 
if (!@$_SESSION['cart']) {
	echo "<script>location='index.php'</script>";
}
$dat = mysql_fetch_assoc(mysql_query("SELECT kota FROM users WHERE id = '$_SESSION[id]'"));
$kurir = mysql_fetch_assoc(mysql_query("SELECT * FROM kurir WHERE kota = '$dat[kota]'"));
$user = mysql_fetch_assoc(mysql_query("SELECT * FROM users WHERE id = '$_SESSION[id]'"));
?>
<div role="main" class="main">
	<div class="cart">
		<div class="container">
			<div class="checkout-menu clearfix">
				<div class="dropdown pull-right checkout-review-dropdown">
					<button class="btn btn-primary mb-sm" id="reviewTable" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-shopping-cart"></i> Rp. 255.000</button>
					<div class="dropdown-menu" aria-labelledby="reviewTable">
						<h3>Rincian Pesanan <span class="text-muted font-weight-normal">(<?php echo count(@$_SESSION["cart"]); ?> item)</span></h3>
						<table>
							<thead>
								<tr>
									<th>Produk</th>
									<th class="text-center">Kuantitas</th>
									<th class="text-right">Harga</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$totalharga= 0;
								if(is_array(@$_SESSION["cart"])){
								foreach (@$_SESSION["cart"] as $id_produk => $jumlah) {
								$qry = mysql_query("SELECT * FROM produk WHERE id = '$id_produk'");
								$i = mysql_fetch_assoc($qry);
								$total = $i["harga"]*$jumlah;
								$totalharga+=$total
								?>
								<tr>
									<td><?php echo $i['nama']; ?></td>
									<td class="text-center"><?php echo $jumlah; ?></td>
									<td class="text-right"><?php echo rupiah($i['harga']); ?></td>
								</tr>
								<?php }
								} ?>
								<tr>
									<td colspan="3"><strong>Pengiriman Standar</strong><br>Jum'at, 13 - Senin, 16 Okt 2017</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2">Subtotal<br><small class="text-success">Ongkos Kirim - <?php echo $kurir['kota']; ?></small></td>
									<td class="text-right"><?php echo rupiah($totalharga); ?><br><small class="text-success"><?php echo rupiah($kurir['harga']); ?></small></td>
								</tr>
								<tr>
									<td colspan="2"><strong>Total</strong><br></td>
									<td class="text-danger text-right"><?php echo rupiah($totalharga+$kurir['harga']); ?></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<form method="post" action="action.php?kirim=checkout">
				<div class="row">
				<div class="col-md-6">
					<div class="form-col">
						<h3>Nama dan Alamat</h3>
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<div class="form-group">
									<label>Nama <span class="required">*</span></label>
									<input type="text" name="nama" value="<?php echo $user['nama']; ?>" class="form-control" required>
									<input type="hidden" required name="id_user" value="<?php echo $user['id']; ?>">
									<input type="hidden" name="id_kurir" value="<?php echo $user['id']; ?>">
									<input type="hidden" name="totalharga" value="<?php echo $totalharga+$kurir['harga']; ?>">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<div class="form-group">
									<label>Alamat <span class="required">*</span></label>
									<textarea rows="5" required name="alamat" class="form-control"><?php echo $user['alamat']; ?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Provinsi <span class="required">*</span></label>
											<input type="text" name="provinsi" value="<?php echo $user['provinsi']; ?>" class="form-control" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Kode Pos <span class="required">*</span></label>
											<input type="text" name="kode_pos" value="<?php echo $user['kode_pos']; ?>" class="form-control" required>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Kota <span class="required">*</span></label>
											<input type="text" name="kota" value="<?php echo $user['kota']; ?>" class="form-control" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Nomor Telpon <span class="required">*</span></label>
											<input type="text" name="nomor" value="<?php echo $user['nomor']; ?>" class="form-control" readonly>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-col">
						<h3>Metode Pembayaran</h3>
						<div class="checkout-payment-method">

							<?php 
							$q = mysql_query("SELECT * FROM bank");
							while ($data = mysql_fetch_assoc($q)) { ?>
							<div class="radio">
								<label>
									<input type="radio" required name="transfer" class="payment-card-check" value="<?php echo $data['id']; ?>">a/n <?php echo $data['nama']; ?> - <?php echo $data['no_rek']; ?> - <?php echo $data['bank']; ?>
								</label>
							</div>
							<?php } ?>

						</div>
						<h3 class="mt-lg">Informasi Pengiriman</h3>
						<p>Pengiriman Standar: Rp. 5.000</p>
						<p>Dapatkan pada Jum'at, 13 - Senin, 16 Okt 2017</p>
					</div>
					<button class="btn btn-primary mb-sm" ><i class="fa fa-shopping-cart"></i> Beli Sekarang</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>