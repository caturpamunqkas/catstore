<div role="main" class="main">
			<section class="form-section">
				<div class="container">
					<h1 class="h2 heading-primary font-weight-normal mb-md mt-lg">Buat Akun Baru</h1>
					<div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
						<div class="box-content">
							<form action="do_daftar.php" method="post">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="font-weight-normal">Nama Lengkap <span class="required">*</span></label>
											<input type="text" name="register_nama" class="form-control" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="font-weight-normal">Username <span class="required">*</span></label>
											<input type="text" name="register_username" class="form-control" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="font-weight-normal">Email <span class="required">*</span></label>
											<input type="email" name="register_email" class="form-control" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="font-weight-normal">Telepon <span class="required">*</span></label>
											<input type="number" name="register_telepon" class="form-control" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="font-weight-normal">Alamat <span class="required">*</span></label>
											<input type="text" name="register_alamat" class="form-control" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="font-weight-normal">Provinsi <span class="required">*</span></label>
											<input type="text" name="register_provinsi" class="form-control" required>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="font-weight-normal">Kota / Kabupaten <span class="required">*</span></label>
											<input type="text" name="register_kota" class="form-control" required>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="font-weight-normal">Kode Pos <span class="required">*</span></label>
											<input type="number" name="register_kode_pos" class="form-control" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="font-weight-normal">Kata Sandi</label>
											<input type="password" name="register_password" class="form-control" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-action clearfix mt-none">
											<a href="index.php?i=login" class="pull-left"><i class="fa fa-angle-double-left"></i> Sudah punya akun? Masuk di sini.</a>
											<button type="submit" class="btn btn-primary">Daftar Akun</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
		</div>