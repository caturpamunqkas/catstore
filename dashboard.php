<div role="main" class="main">

	<div class="container">
		<h2 class="slider-title">
			<span class="inline-title">Paling Baru</span>
			<span class="line"></span>
		</h2>
		<div class="owl-carousel owl-theme manual featured-products-carousel">
			<?php
			$qry = mysql_query("SELECT A.foto, A.nama, A.harga, A.id, B.nama AS nama_kat FROM produk AS A INNER JOIN kategori AS B ON (A.id_kategori = B.id) WHERE status='' AND stok>0 ORDER BY A.id DESC LIMIT 5");
	  		while ($row = mysql_fetch_assoc($qry)) {
			?>
			<div class="product">
				<figure class="product-image-area">
					<a href="index.php?detail=<?php echo $row['id']?>" class="product-image" title="Nama Produk">
						<img src="includes/images/produk/<?php echo $row['foto']; ?>" alt="Nama Produk">
					</a>
					<a href="index.php?detail=<?php echo $row['id']?>" class="product-quickview">
						<i class="fa fa-share-quare-o"></i>
						<span>Quick View</span>
					</a>
					<!-- <div class="product-label"><span class="discount">-10%</span></div> -->
				</figure>
				<div class="product-details-area">
					<h2 class="product-name"><a href="javascript:void(0)" title="Nama Produk"><?php echo $row['nama']; ?></a></h2>
					<div class="product-ratings">
						<!-- <div class="ratings-box">
							<div class="rating" style="width: 60%;"></div>
						</div> -->
						<?php echo $row['nama_kat']; ?>
					</div>
					<div class="product-price-box">
						<!-- <span class="old-price">Rp. 550.000</span> -->
						<span class="product-price"><?php echo rupiah($row['harga']); ?></span>
					</div>
					<div class="product-actions">
						<a href="javascript:void(0)" onclick="addtocart(<?php echo $row['id']; ?>)" class="addtocart" title="Tambahkan ke Keranjang">
							<span>Tambahkan ke Keranjang</span>
						</a>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>