<?php 
$qry = mysql_query("SELECT * FROM produk WHERE id = '$_GET[detail]'");
$data = mysql_fetch_assoc($qry);

$i = mysql_query("SELECT * FROM kategori WHERE id = '$data[id_kategori]'");
$kat = mysql_fetch_assoc($i);

if (@$_SESSION['id']) {
	$nama = mysql_fetch_assoc(mysql_query("SELECT nama FROM users WHERE id = '$_SESSION[id]'"));
}
?>
<div role="main" class="main">
	<section class="page-header">
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="index.html">Home</a></li>
				<li><a href="index.php?kategori=<?php echo $kat['id'] ?>"><?php echo $kat['nama'] ?></a></li>
				<li class="active"><?php echo $data['nama'] ?></li>
			</ul>
		</div>
	</section>

	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="product-essential">
					<div class="row">
						<div class="product-img-box col-sm-5">
							<div class="product-img-box-wrapper">
								<div class="product-img-wrapper">
									<img src="includes/images/produk/<?php echo $data['foto']; ?>" id="product-zoom" alt="Gambar Produk">
								</div>
								<a href="javascript:void(0)" class="product-img-zoom" title="Zoom">
									<span class="glyphicon glyphicon-search"></span>
								</a>
							</div>
						</div>
						<div class="product-details-box col-sm-7">
							<h1 class="product-name"><?php echo $data['nama']; ?></h1>
							
							<div class="product-short-desc">
								<!-- <p><span class="bold">1 Bulan</span> Garansi</p> -->
								<div class="row">
									<div class="col-md-6">
										<p style="margin-left: 15px"><?php echo $data['deskripsi']; ?></p>
									</div>
								</div>
							</div>
							<div class="product-detail-info">
								<div class="product-price-box">
									<span class="product-price"><?php echo rupiah($data['harga']); ?></span>
								</div>
								<p class="availability">
									<span class="font-weight-semibold">Status:</span> <?php echo $data['stok']; ?> Stock<br>
									<span class="font-weight-semibold">Cicilan:</span> 3 bulan, hanya Rp. 25.000/bulan.
								</p>
							</div>
							<div class="product-actions">
								<div class="product-detail-qty">
									<input type="text" class="vertical-spinner" id="product-vqty" value="1">
								</div>
								<a href="javascript:void(0)" onclick="addtocart(<?php echo $data['id']; ?>)" class="addtocart" title="Tambahkan ke Keranjang"><span>Tambahkan ke Keranjang</span></a>
							</div>
						</div>
					</div>
				</div>
				<div class="tabs product-tabs">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#product-rev" data-toggle="tab">Ulasan</a></li>
					</ul>
					<div class="tab-content">
						<div id="product-rev" class="tab-pane active">
							<div class="pull-right">
								<button class="btn btn-primary" id="review-btn">Tulis Ulasan Anda</button>
							</div>
							<?php if (@$_SESSION['status']=='userLogin') { ?>
								<div class="add-product-review hide">
									<hr class="taller mb-lg">
									<h3 class="text-uppercase heading-text-color font-weight-semibold">Tulis Ulasan Anda</h3>
									<p>Berapa penilaian Anda tentang produk ini? *</p>
									<form role="form" method="post" action="action.php?kirim=ulasan">
										<div class="form-group">
											<label>Nama <span class="required">*</span></label>
											<input type="text" name="nama" value="<?php echo $nama['nama'];?>" class="form-control" required>
											<input type="hidden" name="id_user" value="<?php echo @$_SESSION[id];?>" class="form-control" required>
											<input type="hidden" name="id_produk" value="<?php echo @$_GET[detail];?>" class="form-control">
										</div>
										<div class="form-group">
											<label>Ulasan Anda <span class="required">*</span></label>
											<textarea cols="5" rows="6" name="ulasan" class="form-control"></textarea>
										</div>
										<div class="text-right">
											<button type="submit" name="submit" class="btn btn-primary">Kirim Ulasan</button>
										</div>
									</form>
									<hr>
								</div>
							<?php }else{ ?>
								<div class="add-product-review hide">
									<hr class="taller mb-lg">
									<h3 class="text-uppercase heading-text-color font-weight-semibold center">Silahkan Login Terlebih Dahulu</h3>
									<hr>
								</div>
							<?php } ?>
							<div class="product-desc-area">
								<ul class="comments">
									<li>
										<?php
										$q = mysql_query("SELECT tanggal, ulasan, nama AS nama_user, foto FROM ulasan AS A JOIN users AS B ON(A.id_user=B.id) WHERE id_produk = '$_GET[detail]' ORDER BY A.id DESC LIMIT 5");

										if (mysql_num_rows($q)!='0') { 
											while ($data = mysql_fetch_array($q)) { ?>
											 	<div class="comment">
													<div class="img-thumbnail">
														<img src="includes/images/users/<?php echo $data['foto']; ?>" class="avatar" alt="Foto Profil">
													</div>
													<div class="comment-block">
														<div class="comment-arrow"></div>
														<span class="comment-by">
															<strong><?php echo $data['nama_user']; ?></strong>
															<span class="product-rating-container pull-right">
																<?php echo tgl_indo($data['tanggal']); ?>
															</span>
														</span>
														<p><?php echo $data['ulasan']; ?></p>
													</div>
												</div>
											<?php } ?>
											
										<?php }else{ ?>
										<li>
											<div class="col-md-12">
												<h3 class="text-uppercase heading-text-color font-weight-semibold">Belum Ada Ulasan</h3>
											</div>
										</li>
										<?php } ?>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<aside class="col-md-3 sidebar product-sidebar" style="background-color: #f4f7f8; padding-top: 10px; padding-bottom: 5px;">
				<p class="mb-none">Dikirim ke</p>
				<p><span class="bold">
				<?php if (@$_SESSION['status']=='userLogin') {
					$qry = mysql_fetch_assoc(mysql_query("SELECT alamat FROM users WHERE id = '$_SESSION[id]'"));
					echo $qry['alamat'];
				}else{
					echo "Alamat Anda";
				} ?>
				</span></p>
				<hr class="solid short">
				<div class="feature-box feature-box-style-3">
					<div class="feature-box-icon">
						<i class="fa fa-truck"></i>
					</div>
					<div class="feature-box-info">
						<h4>PENGIRIMAN</h4>
						<p class="mb-none"><span class="bold">Standar:</span> Selasa 10 - Rabu 11 Okt</p>
					</div>
				</div>
				<div class="feature-box feature-box-style-3">
					<div class="feature-box-icon">
						<i class="fa fa-money"></i>
					</div>
					<div class="feature-box-info">
						<h4>PEMBAYARAN</h4>
						<p class="mb-none">Bayar di Tempat berlaku</p>
					</div>
				</div>
			</aside>
		</div>
	</div>
</div>