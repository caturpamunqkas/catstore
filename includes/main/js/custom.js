$(document).ready(function() {
	$("#span-grid-view").click(function() {
		$("#span-grid-view").addClass("active");
		$("#span-list-view").removeClass("active");
		$("#grid-view").removeClass("hide");
		$("#list-view").addClass("hide");
	});
	$("#span-list-view").click(function() {
		$("#span-grid-view").removeClass("active");
		$("#span-list-view").addClass("active");
		$("#list-view").removeClass("hide");
		$("#grid-view").addClass("hide");
	});
	$("#review-btn").click(function() {
		$(".add-product-review").toggleClass("hide");
	})
	$("#provinsi").click(function() {
		var chooseProvince = document.getElementById("chooseProvince");
		chooseProvince.innerHTML = '<a href="javascript:void(0)" id="provinsiFirst">DKI Jakarta</a> > Pilih Kota';
		$("#listProvinsi").addClass("hide");
		$("#listKota").removeClass("hide");
	});
	$("#kota").click(function() {
		var chooseCity = document.getElementById("chooseProvince");
		chooseCity.innerHTML = '<a href="javascript:void(0)" id="provinsiFirst">DKI Jakarta</a> > <a href="javascript:void(0)" id="provinsiSecond">Kota Jakarta Pusat</a> > Pilih Kota';
		$("#listKota").addClass("hide");
		$("#listTempat").removeClass("hide");
	});
	$("#tempat").click(function() {
		$("#changeAddress").removeClass("in").removeAttr("style");
		$("div").removeClass("modal-backdrop fade in");
	});
});