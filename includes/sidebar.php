<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $_SESSION['nama'];?></div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="<?php if(@$_GET['i']=='dashboard'){ echo 'active';}?>"><a href="index.php?i=dashboard">	<em class="fa fa-dashboard">&nbsp;</em> Dashboard</a>
			</li>
			<li class="<?php if(@$_GET['i']=='transaksi' || @$_GET['action']=='transaksi_detail'){ echo 'active';}?>"><a href="index.php?i=transaksi">
				<em class="fa fa-calendar">&nbsp;</em> Transaksi</a>
			</li>
			<li class="<?php if(@$_GET['i']=='kategori'){ echo 'active';}?>"><a href="index.php?i=kategori">	<em class="fa fa-calendar">&nbsp;</em> Kategori</a>
			</li>
			<li class="<?php if(@$_GET['i']=='produk'){ echo 'active';}?>"><a href="index.php?i=produk">
				<em class="fa fa-calendar">&nbsp;</em> Produk</a>
			</li>
			<li class="<?php if(@$_GET['i']=='ulasan'){ echo 'active';}?>"><a href="index.php?i=ulasan">
				<em class="fa fa-calendar">&nbsp;</em> Ulasan</a>
			</li>
			<li class="parent <?php if(@$_GET['i']=='bank' || @$_GET['i']=='kurir'){ echo 'active';}?>"><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-navicon">&nbsp;</em> Config <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="<?php if(@$_GET['i']=='kurir'){ echo 'active';}?>" href="index.php?i=kurir">
						<span class="fa fa-arrow-right">&nbsp;</span> Kurir
					</a></li>
					<li><a class="<?php if(@$_GET['i']=='bank'){ echo 'active';}?>" href="index.php?i=bank">
						<span class="fa fa-arrow-right">&nbsp;</span> Bank Transfer
					</a></li>
					<li><a class="<?php if(@$_GET['i']=='config'){ echo 'active';}?>" href="index.php?i=config">
						<span class="fa fa-arrow-right">&nbsp;</span> Config Website
					</a></li>
				</ul>
			</li>
			<li><a href="do_logout.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->