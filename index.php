<?php
include 'includes/koneksi.php';
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="Aksamedia.co.id">
	<title>Catstore</title>
	<link rel="shortcut icon" type="image/x-icon" href="includes/main/img/favicon.ico">
	<link rel="apple-touch-icon" href="includes/main/img/apple-touch-icon.png">

	<link rel="stylesheet" type="text/css" href="includes/main/https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light">

	<!-- Vendor CSS -->
	<link rel="stylesheet" type="text/css" href="includes/main/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="includes/main/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="includes/main/vendor/animate/animate.min.css">
	<link rel="stylesheet" type="text/css" href="includes/main/vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" type="text/css" href="includes/main/vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="includes/main/vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="includes/main/vendor/magnific-popup/magnific-popup.min.css">

	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="includes/main/css/theme.css">
	<link rel="stylesheet" type="text/css" href="includes/main/css/theme-elements.css">
	<link rel="stylesheet" type="text/css" href="includes/main/css/theme-blog.css">
	<link rel="stylesheet" type="text/css" href="includes/main/css/theme-shop.css">

	<!-- Current Page CSS -->
	<link rel="stylesheet" type="text/css" href="includes/main/vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" type="text/css" href="includes/main/vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="includes/main/vendor/rs-plugin/css/navigation.css">

	<!-- Skin CSS -->
	<link rel="stylesheet" type="text/css" href="includes/main/css/skins/skin-shop-7.css">

	<!-- Demo CSS -->
	<link rel="stylesheet" type="text/css" href="includes/main/css/demos/demo-shop-7.css">

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" type="text/css" href="includes/main/css/custom.css">

	<!-- Head Libs -->
	<script type="text/javascript" src="includes/main/vendor/modernizr/modernizr.min.js"></script>
	<style>
		img.profile 
		{
		  border: 1px solid #ddd;
		  border-radius: 4px;
		  padding: 5px;
		  width: 150px;
		  border-radius: 10%;
		}
	</style>
</head>

<body>
	<div class="body">
		<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyStartAt': 147, 'stickySetTop': '-41px', 'stickyChangeLogo': false}">
			<div class="header-body">
				<div class="header-top">
					<div class="container">
						<div class="dropdowns-container">
							<p class="welcome-msg">Selamat datang, 
							<?php 
							if (@$_SESSION['status']=='userLogin') { 
								 echo @$_SESSION['nama'];
							}else{
								echo "Pengunjung";
							} ?>
							</p>
						</div>
						<!-- Top Right Menu -->
						<div class="top-menu-area">
							<a href="includes/main/javascript:void(0)">Menu Lainnya <i class="fa fa-caret-down"></i></a>
							<ul class="top-menu">
								<?php
								if (@$_SESSION['status']=='userLogin') { 
									echo "<li><a href='?akun=index'>Dashboard</a></li>
									<li><a href='do_logout.php'>Logout</a></li>";
								}else{
									echo "<li><a href='?i=login'>Login</a></li>
									<li><a href='?i=daftar'>Daftar</a></li>";
								} ?>
							</ul>
						</div>
						<!-- ./Top Right Menu -->
					</div>
				</div>

				<div class="header-container container">
					<div class="header-row">
						<div class="header-column">
							<div class="header-logo">
								<a href="index.php">
									<img src="includes/main/img/demos/shop/logo-shop-charcoal.png" alt="Bumbee Logo" width="108" height="50">
								</a>
							</div>
						</div>
						<div class="header-column">
							<div class="row">
								<div class="cart-area">
									<?php include 'cart-side.php'; ?>
								</div>

								<a href="includes/main/javascript:void(0)" class="mmenu-toggle-btn" title="Toggle Menu">
									<i class="fa fa-bars"></i>
								</a>

								<div class="header-search">
									<a href="includes/main/javascript:void(0)" class="search-toggle"><i class="fa fa-search"></i></a>
									<form action="#" method="GET">
										<div class="header-search-wrapper">
											<input type="text" name="q" class="form-control" placeholder="Cari produk, kategori, atau merk">
											<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
										</div>
									</form>
								</div>

								<div class="header-nav-main">
									<nav>
										<ul class="nav nav-pills" id="mainNav">
											<?php
											  	$qry = mysql_query("SELECT * FROM kategori"); 
										  		while ($row = mysql_fetch_assoc($qry)) {
										  	?>
											<li class="dropdown">
												<a href="index.php?kategori=<?php echo $row['id'];?>" class=""><?php echo $row['nama'];?></a>
											</li>
											<?php } ?>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div id="mobile-menu-overlay"></div>

		<?php 
		if (@$_GET['i']=='index') {
			include 'dashboard.php';
		}elseif (@$_GET['kategori']) {
			include 'kategori.php';
		}elseif (@$_GET['detail']) {
			include 'detail.php';
		}elseif (@$_GET['i']=='cart') {
			include 'cart.php';
		}elseif (@$_GET['i']=='checkout') {
			include 'checkout.php';
		}elseif (@$_GET['i']=='login' && !@$_SESSION['status']) {
			include 'login.php';
		}elseif (@$_GET['i']=='daftar') {
			include 'daftar.php';
		}elseif (@$_GET['akun']=='index' && @$_SESSION['status']) {
			include 'user_akun.php';
		}elseif (@$_GET['akun']=='info' && @$_SESSION['status']) {
			include 'user_info.php';
		}elseif (@$_GET['akun']=='nota' && @$_SESSION['status'] && @$_GET['id']) {
			include 'user_nota.php';
		}else{
			include 'dashboard.php';
		}
		?>
		<!-- ./Modal Lacak Pengiriman -->

		<footer id="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-3 margin-at-small-only">
						<div class="row">
							<div class="footer-ribbon">
								<span>Hubungi Kami</span>
							</div>
						</div>
						<div class="contact-details">
							<?php
							$data = mysql_fetch_assoc(mysql_query("SELECT * FROM config WHERE id='1'"));
							?>
							<ul class="contact">
								<li>
									<p>
										<i class="fa fa-map-marker"></i> <strong>Alamat:</strong>
										<br>
										<?php echo $data['alamat']; ?>
									</p>
								</li>
								<li>
									<p>
										<i class="fa fa-phone"></i> <strong>No. Telepon:</strong>
										<br>
										<?php echo $data['telepon']; ?>
									</p>
								</li>
								<li>
									<p>
										<i class="fa fa-envelope"></i> <strong>Email:</strong>
										<br>
										<a href="mailto:<?php echo $data['email']; ?>"><?php echo $data['email']; ?></a>
									</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<!-- Vendor -->
	<script src="includes/main/vendor/jquery/jquery.min.js"></script>
	<script src="includes/main/vendor/jquery.appear/jquery.appear.min.js"></script>
	<script src="includes/main/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="includes/main/vendor/jquery-cookie/jquery-cookie.min.js"></script>
	<script src="includes/main/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="includes/main/vendor/common/common.min.js"></script>
	<script src="includes/main/vendor/jquery.validation/jquery.validation.min.js"></script>
	<script src="includes/main/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
	<script src="includes/main/vendor/jquery.gmap/jquery.gmap.min.js"></script>
	<script src="includes/main/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
	<script src="includes/main/vendor/isotope/jquery.isotope.min.js"></script>
	<script src="includes/main/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="includes/main/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="includes/main/vendor/vide/vide.min.js"></script>
	
	<!-- Theme Base, Components and Settings -->
	<script src="includes/main/js/theme.js"></script>
	<script src="includes/main/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="includes/main/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

	<!-- Current Page Vendor and Views -->
	<script src="includes/main/js/views/view.contact.js"></script>

	<!-- Demo -->
	<script src="includes/main/js/demos/demo-shop-7.js"></script>

	<!-- Theme Custom -->
	<script src="includes/main/js/custom.js"></script>

	<!-- Theme Initialization Files -->
	<script src="includes/main/js/theme.init.js"></script>
	<script type="text/javascript">
		function addtocart($id){
			$(document).ready(function(){
			    $.ajax({url: "action.php?i=add&id="+$id, success: function(result){
			      location.reload();
			      alert("Berhasil Menambahkan Ke Keranjang");
			    }});
			});
		}
		function deltocart($id){
			$(document).ready(function(){
			    $.ajax({url: "action.php?i=del&id="+$id, success: function(result){
			      location.reload();
			      alert("Berhasil Menghapus Dari Keranjang");
			    }});
			});
		}

		var i = 0;
		function plus($id) {
		    document.getElementById('inc-'+$id).value = + 1;
		}
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
		  $('#dtBasicExample').DataTable();
		  $('.dataTables_length').addClass('bs-select');
		});
	</script>
</body>
</html>