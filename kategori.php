<div role="main" class="main">
	<section class="page-header">
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="index.php">Home</a></li>
				<li><a href="javascript:void(0)">
				<?php
				$qry = mysql_query("SELECT * FROM kategori WHERE id = '$_GET[kategori]'");
				$data = mysql_fetch_assoc($qry);
				echo $data['nama'];
				?>
				</a></li>
			</ul>
		</div>
	</section>

	<div class="container">
		<div class="row">
			<!-- Grid View Mode -->
			<div class="col-md-12">
				<div class="toolbar">
					<div class="sorter">
						<div class="view-mode">
							<?php
							$qry = mysql_query("SELECT * FROM produk WHERE id_kategori = '$_GET[kategori]'");
					  		$i = mysql_num_rows($qry);
							?>
							<h3><?php echo $data['nama']; ?> <span class="small help-block"><?php echo $i; ?> item ditemukan</span></h3>
						</div>
						<div class="view-mode pull-right">
							<label>Tampilan:</label>
							<a href="javascript:void(0)">
								<i class="fa fa-th active" id="span-grid-view"></i>
							</a>
							<a href="javascript:void(0)">
								<i class="fa fa-list-ul" id="span-list-view"></i>
							</a>
						</div>
					</div>
				</div>

				<!-- Grid View Mode -->
				<ul class="products-grid columns4" id="grid-view">
					<?php
					$qry = mysql_query("SELECT A.foto, A.nama, A.harga, A.id, B.nama AS nama_kat FROM produk AS A INNER JOIN kategori AS B ON (A.id_kategori = B.id) WHERE id_kategori = '$_GET[kategori]'");
			  		while ($row = mysql_fetch_assoc($qry)) {
					?>
					<li>
						<div class="product">
							<figure class="product-image-area">
								<a href="index.php?detail=<?php echo $row['id']?>" class="product-image">
									<img src="includes/images/produk/<?php echo $row['foto']; ?>" alt="Gambar Produk">
								</a>
								<a href="index.php?detail=<?php echo $row['id']?>" class="product-quickview">
									<i class="fa fa-share-square-o"></i>
									<span>Lihat Produk</span>
								</a>
							</figure>
							<div class="product-details-area">
								<h2 class="product-name"><a href="javascript:void(0)" title="Nama Produk"><?php echo $row['nama']; ?></a></h2>
								<div class="product-ratings">
									<?php echo $row['nama_kat']; ?>
								</div>
								<div class="product-price-box">
									<!-- <span class="old-price">Rp. 250.000</span> -->
									<span class="product-price" id="harga"><?php echo rupiah($row['harga']); ?></span>
								</div>
								<div class="product-actions">
									<a href="javascript:void(0)" onclick="addtocart(<?php echo $row['id']; ?>)" class="addtocart" title="Tambahkan ke Keranjang"><span>+ ke Keranjang</span></a>
								</div>
							</div>
						</div>
					</li>
					<?php } ?>
				</ul>
				<!-- ./Grid View Mode -->

				<!-- List View Mode -->
				<ul class="products-list hide" id="list-view">
					<?php
					$qry = mysql_query("SELECT A.foto, A.deskripsi, A.nama, A.harga, A.id, B.nama AS nama_kat FROM produk AS A INNER JOIN kategori AS B ON (A.id_kategori = B.id) WHERE id_kategori = '$_GET[kategori]'");
			  		while ($row = mysql_fetch_assoc($qry)) {
					?>
					<li>
						<div class="product product-list">
							<figure class="product-image-area">
								<a href="index.php?detail=<?php echo $row['id']?>" class="product-image" title="Nama Produk">
									<img src="includes/images/produk/<?php echo $row['foto']; ?>" alt="Nama Produk">
								</a>
							</figure>
							<div class="product-details-area">
								<h2 class="product-name"><a href="index.php?detail=<?php echo $row['id']?>" title="Nama Produk"><?php echo $row['nama']; ?></a></h2>
								<div class="product-ratings">
									Kategori : <?php echo $row['nama_kat']; ?>
								</div>
								<div class="product-short-desc">
									<div class="row">
										<div class="col-md-6">
											<p style="margin-left: 15px"><?php echo $row['deskripsi']; ?></p>
										</div>
									</div>
								</div>
								<div class="product-price-box">
									<span class="product-price"><?php echo rupiah($row['harga']); ?></span>
								</div>
								<div class="product-actions">
									<a href="javascript:void(0)" onclick="addtocart(<?php echo $row['id']; ?>)" class="addtocart" title="Tambahkan ke Keranjang"><span>Tambahkan ke Keranjang</span></a>
									<a href="javascript:void(0)" class="addtowishlist" title="Tambahkan ke Wishlist"><i class="fa fa-heart"></i></a>
									<a href="javascript:void(0)" class="comparelink" title="Bandingkan Produk"><i class="glyphicon glyphicon-signal"></i></a>
									<a href="javascript:void(0)" class="quickview" title="Lihat Produk"><i class="fa fa-search"></i></a>
								</div>
							</div>
						</div>
					</li>
					<?php } ?>
				</ul>
				<!-- ./List View Mode -->

				<div class="toolbar-bottom pull-right">
					<div class="toolbar">
						<div class="sorter">
							<ul class="pagination">
								<li class="active"><a href="javascript:void(0)">1</a></li>
								<li><a href="javascript:void(0)">2</a></li>
								<li><a href="javascript:void(0)">3</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-caret-right"></i></a></li>
							</ul>
							<ul class="limiter">
								<label>Produk per Halaman:</label>
								<select>
									<option>12</option>
									<option>24</option>
									<option>36</option>
								</select>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- ./Grid View Mode -->

		</div>
	</div>
</div>