<div role="main" class="main">
	<section class="form-section">
		<div class="container">
			<h1 class="h2 heading-primary font-weight-normal mb-md mt-lg">Masuk atau Buat Akun Baru</h1>
			<div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
				<div class="box-content">
					<form action="do_login.php" method="post">
						<div class="row">
							<div class="col-md-6">
								<div class="form-content">
									<h3 class="heading-text-color font-weight-normal">Buat Akun Baru</h3>
									<p>Dapatkan banyak promo dan diskon dengan menjadi member BumbeeCollection.</p>
								</div>
								<div class="form-action clearfix">
									<button class="btn btn-primary" onclick="window.location.href='index.php?i=daftar'">Buat Akun Baru</button>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-content">
									<h3 class="heading-text-color font-weight-normal">Masuk Member</h3>
									<p>Sudah menjadi member? Silakan masuk di sini.</p>
									<div class="form-group">
										<label class="font-weight-normal">Username <span class="required">*</span></label>
										<input type="text" name="login_username" class="form-control" required>
									</div>
									<div class="form-group">
										<label class="font-weight-normal">Kata Sandi <span class="required">*</span></label>
										<input type="password" name="login_password" class="form-control" required>
									</div>
								</div>
								<div class="form-action clearfix">
									<a href="javascript:void(0)" class="pull-left">Lupa Kata Sandi?</a>
									<button type="submit" class="btn btn-primary">Masuk</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>