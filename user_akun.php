<?php 
$qry = mysql_query("SELECT * FROM users WHERE username='$_SESSION[username]'");
$data = mysql_fetch_assoc($qry);
?>
<div role="main" class="main">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-push-3">
				<div class="row">
					<h1 class="h2 heading-primary font-weight-normal ml-md">Panel Akun</h1>
					<div class="col-md-6">
						<div class="panel-box">
							<div class="panel-box-title">
								<h3>Informasi Pribadi</h3>
								<a href="javascript:void(0)" class="panel-box-edit">UBAH</a>
							</div>
							<div class="panel-box-content">
								<ul class="list list-unstyled no-mar-bot mb-none">
									<li><?php echo $data['nama']; ?></li>
									<li><?php echo $data['email']; ?></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel-box">
							<div class="panel-box-title">
								<h3>Alamat Pengiriman</h3>
								<a href="javascript:void(0)" class="panel-box-edit">UBAH</a>
							</div>
							<div class="panel-box-content">
								<ul class="list list-unstyled no-mar-bot mb-none">
									<li><?php echo $data['nama']; ?></li>
									<li><?php echo $data['alamat']; ?></li>
									<li><?php echo $data['kota']; ?> - <?php echo $data['provinsi']; ?> - <?php echo $data['kode_pos']; ?></li>
									<li><?php echo $data['nomor']; ?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<h1 class="h2 heading-primary font-weight-normal ml-md">Pesanan Terbaru</h1>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td>Dipesan pada</td>
										<td>Total</td>
										<td>Status</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$qry = mysql_query("SELECT * FROM pembelian WHERE id_user = '$_SESSION[id]' ORDER BY tanggal DESC LIMIT 3");
									while ($data = mysql_fetch_assoc($qry)) { ?>
									<tr>
										<td><?php echo tgl_indo($data['tanggal']); ?></td>
										<td><?php echo rupiah($data['total']); ?></td>
										<td>
											<ul class="list list-unstyled no-mar-bot">
												<?php 
												if ($data['status']=='waiting') {
													echo "<li>Menunggu Pembayaran</li>";
												}elseif ($data['status']=='pending') {
													echo "<li>Menunggu Konfirmasi Admin</li>";
												}elseif ($data['status']=='proses') {
													echo "<li><span class='text-success'>Dikonfirmasi Admin</span></li>
														<li>Barang Akan Dikirim</li>";
												}elseif ($data['status']=='kirim') {
													echo "<li><span class='text-success'>Dikonfirmasi Admin</span></li>
														<li><span class='text-success'>Dalam Pengiriman</span></li>";
												}else{
													echo "<li>Pesanan <strong>DIBATALKAN</strong></li>";
												}
												?>
											</ul>
										</td>
										<td>
											<?php if(!$data['resi']==null) { ?>
												<strong>NO. RESI : </strong><font color="green"><?php echo $data['resi']; ?></font>
												<?php } ?>
										</td>
										<td colspan="2"><a href="index.php?akun=nota&id=<?php echo $data['id']; ?>">DETAIL</a></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?php include 'sidebar.php'; ?>
		</div>
	</div>
</div>