<?php
$qry = mysql_fetch_assoc(mysql_query("SELECT * FROM users WHERE id = '$_SESSION[id]'"));
?>
<div role="main" class="main">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-push-3">
				<div class="row">
					<h1 class="h2 heading-primary font-weight-normal ml-md">Informasi Pribadi</h1>
					<div class="col-md-12">
						<form class="form-horizontal form-bordered" method="post" enctype="multipart/form-data" action="action.php?ubah=user_info">
							<div class="form-group">
								<label class="col-md-3 control-label" for="nama" style="text-align: left;">Foto </label>
								<div class="col-md-6">
									<input type="file" name="foto" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="nama" style="text-align: left;">Nama <span class="required">*</span></label>
								<div class="col-md-6">
									<input type="text" name="nama" id="nama" class="form-control" value="<?php echo $qry['nama']; ?>">
									<input type="hidden" name="id" id="nama" class="form-control" value="<?php echo $qry['id']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="email" style="text-align: left;">Email <span class="required">*</span></label>
								<div class="col-md-6">
									<input type="text" name="email" id="email" class="form-control" value="<?php echo $qry['email']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="nohp" style="text-align: left;">Nomor Handphone <span class="required">*</span></label>
								<div class="col-md-6">
									<input type="text" name="telepon" id="nohp" class="form-control" value="<?php echo $qry['nomor']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="nama" style="text-align: left;">Alamat <span class="required">*</span></label>
								<div class="col-md-6">
									<textarea cols="5" rows="2" name="alamat" class="form-control"><?php echo $qry['alamat']; ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="nama" style="text-align: left;">Provinsi <span class="required">*</span></label>
								<div class="col-md-6">
									<input type="text" name="provinsi" id="nama" class="form-control" value="<?php echo $qry['provinsi']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="dob" style="text-align: left;">Kota <span class="required">*</span></label>
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-4">
											<input type="text" name="kota" id="nama" class="form-control" value="<?php echo $qry['kota']; ?>">
										</div>
										<div class="col-md-4">
											<label class="col-md-3 control-label" style="text-align: right;">Kode<span class="required">*</span></label>
										</div>
										<div class="col-md-4">
											<input type="number" name="kode_pos" id="nama" class="form-control" value="<?php echo $qry['kode_pos']; ?>">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-3 col-md-offset-6">
									<button type="submit" class="btn btn-primary btn-block text-uppercase">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<?php include 'sidebar.php'; ?>
		</div>
	</div>
</div>