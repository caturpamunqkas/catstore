<?php
$qry = mysql_query("SELECT P.tanggal, P.id_user, P.resi, U.nama, K.kota, B.nama AS nama_bank, B.bank, B.no_rek, K.harga, P.total, P.alamat, P.status FROM pembelian AS P 
	JOIN users AS U ON(P.id_user=U.id)
	JOIN bank AS B ON(P.id_bank=B.id)
	JOIN kurir AS K ON(P.id_kurir=K.id) WHERE P.id = '$_GET[id]'");
$pembelian = mysql_fetch_assoc($qry);

?>
<div role="main" class="main">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-push-3">
				<div class="row">
					<div class="col-md-12 col-lg-12">
						<div class="cart-table-wrap">
							<?php if(@$_SESSION['id_user']=$pembelian['id_user'] && mysql_num_rows($qry)) { ?>
								<div class="row">
									<div class="col-md-7">
										<div class="panel-box-content">
											<ul class="list list-unstyled no-mar-bot mb-none">
												<li><strong>NAMA PENERIMA : </strong> <?php echo $pembelian['nama']; ?></li>
												<li><strong>DIKIRIM KE : </strong><?php echo $pembelian['alamat']; ?></li>
												<li><strong>TGL PEMESANAN : </strong><?php echo tgl_indo($pembelian['tanggal']); ?></li>
												<?php if(!$pembelian['resi']==null) { ?>
												<li><strong>NO. RESI : </strong><font color="green"><?php echo $pembelian['resi']; ?></font>
												<?php } ?>
												<li><strong>STATUS KONFIRMASI : </strong> </li>
												<?php 
												if ($pembelian['status']=='waiting') {
													echo "<li>Menunggu Pembayaran</li>";
												}elseif ($pembelian['status']=='pending') {
													echo "<li>Menunggu Konfirmasi Admin</li>";
												}elseif ($pembelian['status']=='proses') {
													echo "<li><span class='text-success'>Dikonfirmasi Admin</span></li>
														<li>Barang Akan Dikirim</li>";
												}elseif ($pembelian['status']=='kirim') {
													echo "<li><span class='text-success'>Dikonfirmasi Admin</span></li>
														<li><span class='text-success'>Dalam Pengiriman</span></li>";
												}else{
													echo "<li>Pesanan <strong>DIBATALKAN</strong></li>";
												}
												?>
											</ul>
										</div>
									</div>
									<div class="col-md-4">
										<div class="panel-box-content">
											<ul class="list list-unstyled no-mar-bot mb-none">
												<li><strong>TRANSFER KE : </strong> <?php echo $pembelian['bank']; ?></li>
												<li><strong>NO. REKENING : </strong><?php echo $pembelian['no_rek']; ?></li>
												<li><strong>A/N : </strong> <?php echo $pembelian['nama_bank']; ?></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- <hr> -->
								<table class="cart-table">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Nama Produk</th>
											<th>Kuantitas</th>
											<th>Harga Produk</th>
											<th>Sub Total</th>
										</tr>
									</thead>
									<tbody>
										<?php $qty = mysql_query("SELECT * FROM qty_pembelian AS qty 
											JOIN produk AS pro ON(qty.id_produk=pro.id) WHERE id_pembelian='$_GET[id]'");
										while ($data = mysql_fetch_assoc($qty)) { 
										$kat = mysql_fetch_assoc(mysql_query("SELECT nama FROM kategori WHERE id='$data[id_kategori]'")); ?>
										<tr>
											<td class="product-image-td">
												<a href="javascript:void(0)" title="Nama Produk">
													<img src="includes/images/produk/<?php echo $data['foto']; ?>" alt="Nama Produk">
												</a>
											</td>
											<td class="product-name-td">
												<h2 class="product-name"><a href="javascript:void(0)" title="Nama Produk"><?php echo $data['nama']; ?></a></h2>
												<p><?php echo $kat['nama']; ?></p>
											</td>
											<td>
												<div class="qty-holder">
													<input type="text" class="qty-input" value="<?php echo $data['qty']; ?>" readonly>
												</div>
											</td>
											<td><?php echo rupiah($data['harga']);?></td>
											<td><?php echo rupiah($data['harga']*$data['qty']);?></td>
										</tr>
										<?php } ?>
										<tr>
											<td colspan="4" class="clearfix">
												ONGKOS KIRIM - <strong><?php echo $pembelian['kota']; ?></strong>
											</td>
											<td class="clearfix">
												<?php echo rupiah($pembelian['harga']);?>
											</td>
										</tr>
										<tr>
											<td colspan="4">
												<strong>TOTAL</strong>
											</td>
											<td>
												<?php echo rupiah($pembelian['total']);?>
											</td>
										</tr>
								</table>
							<?php }else{ ?>
								<span class="center col-md-12"><strong>TIDAK ADA DATA</strong></span>
							<?php } ?>
						</div>
						<?php if($pembelian['status']=='waiting') {?>
							<h2>KIRIMKAN BUKTI TRANSFER ANDA</h2>
							<div class="cart-table-wrap">
								<form class="form-horizontal form-bordered" method="post" enctype="multipart/form-data" action="action.php?kirim=bukti">
									<div class="form-group">
										<label class="col-md-3 control-label" for="nama" style="text-align: left;">Bukti Transfer </label>
										<div class="col-md-6">
											<input type="file" required name="foto" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="nama" style="text-align: left;">Nama <span class="required">*</span></label>
										<div class="col-md-6">
											<input type="text" required name="nama" id="nama" class="form-control">
											<input type="hidden" name="id_pembelian" id="nama" value="<?php echo $_GET['id']; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="nama" style="text-align: left;">No. Rekening <span class="required">*</span></label>
										<div class="col-md-6">
											<input type="number" required name="no_rek" id="nama" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="email" style="text-align: left;">Jumlah Transfer <span class="required">*</span></label>
										<div class="col-md-6">
											<input type="number" required name="jumlah" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="nohp" style="text-align: left;">BANK <span class="required">*</span></label>
										<div class="col-md-6">
											<input type="text" required name="bank" id="nohp" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="nama" style="text-align: left;">Transfer Ke <span class="required">*</span></label>
										<div class="col-md-6">
											<div class="checkout-payment-method">
												<?php 
												$q = mysql_query("SELECT * FROM bank");
												while ($data = mysql_fetch_assoc($q)) { ?>
												<div class="radio">
													<label>
														<input type="radio" name="transfer" class="payment-card-check" required  value="<?php echo $data['id']; ?>">a/n <?php echo $data['nama']; ?> - <?php echo $data['no_rek']; ?> - <?php echo $data['bank']; ?>
													</label>
												</div>
												<?php } ?>

											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-3 col-md-offset-6">
											<button type="submit" name="submit" class="btn btn-primary btn-block text-uppercase">Kirim Bukti</button>
										</div>
									</div>
								</form>
							</div>
						<?php }elseif($pembelian['status']=='kirim') { ?>
							<div class="cart-table-wrap">
								<h2 class="center">NOMOR RESI</h2>
								<h2 class="center"><font color="green"><strong><?php echo $pembelian['resi']; ?></font></strong></h2>
							</div>
						<?php }else{ ?>
						<div class="cart-table-wrap">
							<h2 class="center">TUNGGU INFO LEBIH LANJUT</h2>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php include 'sidebar.php'; ?>
		</div>
	</div>
</div>